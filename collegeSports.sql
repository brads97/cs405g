CREATE DATABASE collegeSports;

USE collegeSports;

-- Create 'players' table and upload data
CREATE TABLE players
        (
        pid INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        firstname VARCHAR(20),
        lastname VARCHAR(20),
        age INT,
        position VARCHAR(20),
        class VARCHAR(20),
        number INT
        );
LOAD DATA LOCAL INFILE './Textfiles/players.txt'
INTO TABLE players
FIELDS TERMINATED BY '\t';

-- Create 'stadiums' table and upload data
CREATE TABLE stadiums
        (
        name VARCHAR(20) NOT NULL PRIMARY KEY,
        address VARCHAR(256),
        seating INT,
        ticket_price FLOAT
        );
LOAD DATA LOCAL INFILE './Textfiles/stadiums.txt'
INTO TABLE stadiums
FIELDS TERMINATED BY '\t';

-- Create 'schedule' table and upload data
CREATE TABLE schedule
        (
        game_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        date DATE,
	home VARCHAR(20),
        away VARCHAR(20),
        location VARCHAR(20),
        time TIME
        );
LOAD DATA LOCAL INFILE './Textfiles/schedule.txt'
INTO TABLE schedule
FIELDS TERMINATED BY '\t';

-- Create 'coach' table and upload data
CREATE TABLE coach
        (
        cid INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        firstname VARCHAR(20),
        lastname VARCHAR(20),
        team VARCHAR(20),
        title VARCHAR(20),
        salary int
        );
LOAD DATA LOCAL INFILE './Textfiles/coaches.txt'
INTO TABLE coach
FIELDS TERMINATED BY '\t';

-- Create 'teams' table and upload data
CREATE TABLE teams
        (
        team_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        location VARCHAR(20),
        name VARCHAR(20),
        mascot VARCHAR(20),
        college VARCHAR(20),
        ranking INT,
        winloss_record FLOAT
        );
LOAD DATA LOCAL INFILE './Textfiles/teams.txt'
INTO TABLE teams
FIELDS TERMINATED BY '\t';

-- Create 'teams' table and upload data
CREATE TABLE login
        (
        username VARCHAR(255) NOT NULL PRIMARY KEY,
        password VARCHAR(255),
        firstname VARCHAR(255),
        lastname VARCHAR(255),
        email VARCHAR(225),
	role VARCHAR(225)
        );
LOAD DATA LOCAL INFILE './Textfiles/login.txt'
INTO TABLE login
FIELDS TERMINATED BY '\t';
